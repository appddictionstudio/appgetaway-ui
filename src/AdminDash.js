import React from 'react'
import AdminList from './AdminList'
import './AdminDash.css'

function AdminDash() {
    return (
        <div className="admin-dash">
            <h1 className="admin__title">Pending Approvals</h1>
            <br></br>
            <AdminList />
        </div>
    )
}

export default AdminDash
