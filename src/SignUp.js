import React from 'react'
import Form from "react-bootstrap/Form";
import Button from '@material-ui/core/Button';
import './SignUp.css'
import { render } from '@testing-library/react';




class SignUp extends React.Component {
    
    render(){
        
        const { handleSubmit, handleChange, accounts } = this.props;

       console.log(this.props)
      return (
        
        <div className="florida" >
            <div className="signup">
                <div className="container">

                    <h1>Create Your Account!</h1>
                    <Form onSubmit={handleSubmit}>
                        <Form.Group>
                        <Form.Label>First Name</Form.Label>
                        <Form.Control
                            type="text"
                            name="firstName"
                            id="firstName"
                            placeholder="Enter First Name"
                            onChange={handleChange}
                            value={accounts.firstName}
                        />
                        </Form.Group>
                        <Form.Group>
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control
                            type="text"
                            name="lastName"
                            id="lastName"
                            placeholder="Enter Last Name"
                            onChange={handleChange}
                            value={accounts.lastName}
                        />
                        </Form.Group>
                        <Form.Group>
                        <Form.Label>Email</Form.Label>
                        <Form.Control
                            type="text"
                            name="email"
                            id="email"
                            placeholder="Enter Email"
                            onChange={handleChange}
                            value={accounts.email}
                        />
                        </Form.Group>
                        <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type="password"
                            name="password"
                            id="password"
                            placeholder="Password"
                            onChange={handleChange}
                            value={accounts.password}
                        />
                        </Form.Group>
                        {/* <Form.Group>
                        <Form.Label>Password Confirmation</Form.Label>
                        <Form.Control
                            type="password"
                            name="password_confirmation"
                            id="password"
                            placeholder="Password Confirmation"
                            onChange={handleChange}
                            value={accounts.password_confirmation}
                        />
                        </Form.Group> */}
                       
                        <Button type="submit" variant="contained" color="primary">
                            Signup
                        </Button>
                    </Form>

            
                </div>
                
            </div>
        </div>
    )
}
}

export default SignUp
