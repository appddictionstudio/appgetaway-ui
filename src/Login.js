import React, { useState } from "react";
import "./Login.css";
import { Link } from "react-router-dom";
import Form from "react-bootstrap/Form";

class Login extends React.Component {

  render(){

    const { loginSubmit, accounts, handleChange } = this.props

    return (
      <div className="login">
      <Link to="/">
        <img
          className="login__logo"
          alt=""
          src="https://gitlab.com/ablanco12/scheduler-ui/-/raw/master/public/assets/appddgetaways_logo_anim.gif"
          />
      </Link>
      <div className="login__container">
        <h1>Sign-in</h1>
        <Form onSubmit={loginSubmit}>
                  <Form.Group>
                  <Form.Label>Email</Form.Label>
                        <Form.Control
                            type="text"
                            name="email"
                            id="email"
                            placeholder="Enter Email"
                            onChange={handleChange}
                            // value={accounts.email}
                            />
                        </Form.Group>
                        <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type="password"
                            name="password"
                            id="password"
                            placeholder="Password"
                            onChange={handleChange}
                            // value={accounts.password}
                            />
                        </Form.Group>

        
        <button className="login__signInButton" type="submit" >Sign In</button>
          
        </Form>
        
        <Link to="/signup">
          <button className="login__registerButton">
            Create your AppGetaway Account
          </button>
        </Link>
      </div>
    </div>
  );
}
}

export default Login
