import React from 'react'
// import MenuList from './MenuList'
import { Link } from "react-router-dom";
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MenuIcon from '@material-ui/icons/Menu';
import './Nav.css'

  
function Nav() {
const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleBack = () =>{
   console.log('Hi')
  }


    return (
        <div className="nav">
            
            <Link to="/">
                <div className="nav__top" >
                    <img className="nav__title" src="https://github.com/ablanco12/scheduler-ui/blob/master/public/assets/appddgetaways_logo-03.png?raw=true" alt="" />
                    
                </div>
            </Link>
            
            <div className="menu">
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
               
                <MenuIcon />
              </Button>
              <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
              >
                <Link to="/member">
                 <MenuItem onClick={handleClose}>My Dashboard</MenuItem>
                </Link>
                <MenuItem onClick={handleClose}>Logout</MenuItem>
              </Menu>
              </div>
             
        </div>
         
    )
}

export default Nav
