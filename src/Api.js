import React, { Component } from 'react'

class Api extends React.Component {

    constructor(props){
    super(props);
    this.state = {
        users: []
    }
}

    fetchUsers(){ /*Getting Users Data*/
        fetch('/api/users')/*Don't know the proper route names yet because can't run Rails s so this could be wrong*/
        .then(res => res.json())
        .then(
            (data) => {
            this.setState({
                users: data.users /*Grabbing nested user data if json objects are data{users{[]}}*/
            })
        })
    };

    componentDidMount(){
        this.users()
    }

    render(){
        return (
            <>
            </>
        )
    }
}

export default Api