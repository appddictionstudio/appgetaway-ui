import React from 'react'
import { DataGrid } from '@material-ui/data-grid';

import Button from '@material-ui/core/Button';
import {Link}  from 'react-router-dom'



const columns = [
    { field: 'id', headerName: 'ID', width: 70 },
    
    {
      field: 'Status',
      headerName: 'Status',
      type: 'string',
      width: 200,
    },
    {
      field: 'Location',
      headerName: 'Location',
      type: 'string',
      width: 200,
    },
    {
      field: 'Dates',
      headerName: 'Dates',
      type: 'string',
      width: 200,
    },
    {
      field: 'Nights',
      headerName: 'Number of Nights',
      type: 'number',
      width: 200,
    },
    {
      field: 'fullName',
      headerName: 'Full name',
      description: 'This column has a value getter and is not sortable.',
      sortable: false,
      width: 160,
      valueGetter: (params) =>
        `${params.getValue('firstName') || ''} ${params.getValue('lastName') || ''}`,
    },
  ];
  
  const rows = [
    { id: 1, lastName: 'Snow', firstName: 'Jon', Status: 'Pending', Location: 'Destin, FL', Dates: '5/22 - 5/25', Nights: 3, Email: 'jsnow@appddictionstudio.com' },
    { id: 2, lastName: 'Lannister', firstName: 'Cersei', Status: 'Pending', Location: 'Austin, TX',Email: 'clannister@appddictionstudio.com' },
    { id: 3, lastName: 'Lannister', firstName: 'Jaime',Status: 'Pending', Location: 'Austin, TX', Email: 'jlannister@appddictionstudio.com' },
    { id: 4, lastName: 'Stark', firstName: 'Arya', Status: 'Pending',Location: 'Destin, FL',Email: 'astark@appddictionstudio.com' },
    { id: 5, lastName: 'Targaryen', firstName: 'Daenerys', Status: 'Pending',Location: 'Destin, FL',Email: 'dtargaryen@appddictionstudio.com' },
    { id: 6, lastName: 'Melisandre', firstName: null,Status: 'Pending',Location: 'Destin, FL', Email: 'melisandre@appddictionstudio.com' },
    { id: 7, lastName: 'Clifford', firstName: 'Ferrara',Status: 'Pending',Location: 'Destin, FL', Email: 'fclifford@appddictionstudio.com' },
    { id: 8, lastName: 'Frances', firstName: 'Rossini',Status: 'Pending', Location: 'Destin, FL',Email: 'rfrances@appddictionstudio.com' },
    { id: 9, lastName: 'Roxie', firstName: 'Harvey',Status: 'Pending', Location: 'Destin, FL',Email: 'hroxie@appddictionstudio.com' },
  ];

function MemberList() {
    return (
        <div className="member-list">
            <div style={{ height: 400, width: '100%' }}>
              <DataGrid rows={rows} columns={columns} pageSize={10} checkboxSelection />
            </div>
            <br></br>
            <br></br>
            <Link to="/choose">
                <Button variant="contained" color="primary">
                    Check Availability
                </Button>
            </Link>
        </div>
    )
}

export default MemberList
