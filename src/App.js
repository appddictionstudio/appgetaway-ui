import React from "react";

import "./App.css";
import Schedule from "./Schedule";
import HendersonBeach from "./HendersonBeach";
import LandingHomePage from "./LandingHomePage";
import Nav from "./Nav";
// import DatePicker from './DatePicker'
import AdminDash from "./AdminDash";
import MemberDash from "./MemberDash";
import ConfirmationScreen from "./ConfirmationScreen";
import SignUp from "./SignUp";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  withRouter
} from "react-router-dom";
import Login from "./Login";
import "bootstrap/dist/css/bootstrap.min.css";
import ChooseDestination from "./ChooseDestination";

class App extends React.Component {
  state = {
    accounts: {
      firstName: "",
      lastName: "",
      email: "",
      password: "",
      // password_confirmation: ""
    },
    users: {
      firstName: "",
      lastName: "",
      email: "",
    },
    destinations: {
      address: "",
      city: "",
      state: "",
      zip: "",
    },
    reservations: {
      // start_date: {date: Date()},
      // end_date: {date: Date()},
      start_date: "",
      end_date: "",
      total_days: "",
    },
    pending_approvals: {
      status: "",
    },
  };

  //GET REQUESTS
  fetchUsers() {
    /*Getting Users Data to render when logging in*/
    fetch(
      "/api/users"
    ) /*Don't know the proper route names yet because can't run Rails s so this could be wrong*/
      .then((res) => res.json())
      .then((data) => {
        this.setState({
          users:
            data.users /*Grabbing nested user data if json objects are data{users{[]}}*/,
        });
      });
  }

  fetchDestination() {
    /*Getting Destination Data to render when checking destinations*/
    fetch(
      "/api/v1/destinations"
    ) /*Don't know the proper route names yet because can't run Rails s so this could be wrong*/
      .then((res) => res.json())
      .then((data) => {
        this.setState({
          destinations:
            data.destinations /*Grabbing nested destination data if json objects are data{destinations{[]}}*/,
        });
      });
  }

  fetchReservations() {
    /*Getting Reservation Data to render when checking reservations*/
    fetch(
      "/api/v1/reservations"
    ) /*Don't know the proper route names yet because can't run Rails s so this could be wrong*/
      .then((res) => res.json())
      .then((data) => {
        this.setState({
          reservations:
            data.reservations /*Grabbing nested reservations data if json objects are data{reservations{[]}}*/,
        });
      });
  }

  fetchPendingApprovals() {
    /*Getting Pending_Approvals Data to render when checking reservations*/
    fetch(
      "/api/v1/pending_approvals"
    ) /*Don't know the proper route names yet because can't run Rails s so this could be wrong*/
      .then((res) => res.json())
      .then((data) => {
        this.setState({
          pending_approvals:
            data.pending_approvals /*Grabbing nested pending_approvals data if json objects are data{pending_approvals{[]}}*/,
        });
      });
  }

  loginSubmit = (event) => {
    event.preventDefault();
    const formData = new FormData();
    const { accounts } = this.state;
    const email = accounts.email;
    const password = accounts.password;

    // const cPassword = accounts.password_confirmation;

    formData.append("email", email);
    formData.append("password", password);
    // formData.append("password_confirmation", cPassword);

    // console.log("this is the stuff you have on form", event);
    fetch("https://localhost:3001/api/login", {
      method: "POST",
      headers: {
        Authorization: "Bearer",
      },
      body: formData,
    })
      .then((resp) => resp.json())
      .then((resp) => {
        console.log("this is the sign up info -------->", resp);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const formData = new FormData();
    const { accounts } = this.state;
    const firstName = accounts.firstName;
    const lastName = accounts.lastName;
    const email = accounts.email;
    const password = accounts.password;

    // const cPassword = accounts.password_confirmation;

    formData.append("firstName", firstName);
    formData.append("lastName", lastName);
    formData.append("email", email);
    formData.append("password", password);
    // formData.append("password_confirmation", cPassword);

    // console.log("this is the stuff you have on form", event);
    fetch("https://localhost:3001/api/signup", {
      method: "POST",
      headers: {
        Authorization: "Bearer",
      },
      body: formData,
    })
      .then((resp) => resp.json())
      .then((resp) => {
        console.log("this is the sign up info -------->", resp);
      });
  };

  handleChange = (event) => {
    const accounts = { ...this.state.accounts };
    accounts[event.currentTarget.name] = event.currentTarget.value;
    this.setState({ accounts });
    console.log("firing", accounts);
  };

  render() {
    return (
      <Router>
        <div className="App">
          <Switch>
            <Route exact path="/">
              <Nav />
              <LandingHomePage />
            </Route>
            <Route exact path="/choose-destination">
              <Nav />
              <ChooseDestination />
            </Route>
            <Route path="/login">
              <Login
                loginSubmit={this.loginSubmit}
                handleChange={this.handleChange}
              />
            </Route>
            <Route path="/signup">
              <Nav />
              <SignUp
                accounts={this.state.accounts}
                handleSubmit={this.handleSubmit}
                handleChange={this.handleChange}
              />
            </Route>
            <Route path="/lake-travis">
              <Nav />
              <Schedule />
            </Route>
            <Route path="/henderson-beach">
              <Nav />
              <HendersonBeach />
            </Route>
            <Route path="/admin">
              <Nav />
              <AdminDash />
            </Route>
            <Route path="/member">
              <Nav />
              <MemberDash />
            </Route>
            <Route path="/confirmation">
              <Nav />
              <ConfirmationScreen />
            </Route>
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
