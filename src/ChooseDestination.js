import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom'
import './ChooseDestination.css'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    
  },
  paper: {
    padding: theme.spacing(3),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    height: '100%'
  },
}));

export default function ChooseDestination() {
  const classes = useStyles();

  function FormRow() {
    return (
      <div className ="choose__container" >
        <Grid item xs={6}>
          <Paper className={classes.paper}>
          <div className='bg-image hover-zoom' data-ripple-color='light' >
                <img src='https://gitlab.com/ablanco12/scheduler-ui/-/raw/master/public/assets/LakeTravisHudson.JPG' className='w-100' alt=""/>
                <Link to='/lake-travis'>
                    <div className='mask' style={{ backgroundColor: 'rgba(0, 0, 0, 0.4)' }}>
                    <div className='d-flex justify-content-center align-items-center h-100'>
                        <p className='text-white mb-0'>Lake Travis in Austin, TX</p>
                    </div>
                    </div>
                    <div className='hover-overlay'>
                    <div className='mask' style={{ backgroundColor: 'rgba(251, 251, 251, 0.2)' }}></div>
                    </div>
                </Link>
          </div>
          </Paper>
        </Grid>
        <Grid item xs={6}>
          <Paper className={classes.paper}>
            <div className='bg-image hover-zoom' data-ripple-color='light' >
                <img src='https://bluerockresidential.com/wp-content/uploads/2018/04/ThePreserveHendersonBeach-03.jpg' className='w-100' alt=""/>
                <Link to='/henderson-beach'>
                    <div className='mask' style={{ backgroundColor: 'rgba(0, 0, 0, 0.4)' }}>
                        {/* <div className='d-flex justify-content-center align-items-center h-100'> */}
                            <p className='text-white mb-0'>Henderson Beach in Destin, FL</p>
                        {/* </div> */}
                    </div>
                    <div className='hover-overlay'>
                    <div className='mask' style={{ backgroundColor: 'rgba(251, 251, 251, 0.2)' }}></div>
                    </div>
                </Link>
            </div>
          </Paper>
        </Grid>
        
      </div>
    );
  }
    return (
        <div className={classes.root}>
            <br></br>
            <h1 className="choose__title">Choose Destination</h1>
        <Grid container spacing={1}>
            <Grid container item xs={12}>
            <FormRow />
            </Grid>
        </Grid>
        </div>
    )
}


