import React, { Component } from "react";
import "./LandingHomePage.css";
import { Link } from "react-router-dom";
// import Button from "@material-ui/core/Button";
import Button from '@mui/material/Button';

export default class LandingHomePage extends Component {
  render() {
    return (
      <div className="landing-title">
        <div className="landing-content">
          <div className="landing-text">
                <h1 className="landing-message">Work</h1>
                <h1 className="landing-message">Hard</h1>
            <div class="row ${1| ,row-cols-2,row-cols-3, auto,justify-content-md-center,|}">
              <br/>
              <br/>
            </div>
                <h1 className="landing-message">Play</h1>
                <h1 className="landing-message">Harder!</h1>
          </div>
          <br/>
          <br/>
          <Link to="/choose-destination">
            < Button
              classname="landing-message-button"
              style={{ color: "white" }}
              variant="contained" >
              Book now!
            </ Button>
          </Link>
        </div>
      </div>
    );
  }
}
