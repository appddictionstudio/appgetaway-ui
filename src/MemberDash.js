import React from 'react'
import MemberList from './MemberList'
import './MemberDash.css'

function MemberDash() {
    return (
        <div className="member-dash">
            <h1 className="member__title">My Submissions</h1>
            <MemberList />
        </div>
    )
}

export default MemberDash
