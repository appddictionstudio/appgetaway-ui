import React,{useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Button,Modal } from 'react-bootstrap';
import Paper from '@material-ui/core/Paper';
import axios from 'axios'
import { Link } from 'react-router-dom';



// const BASEURL = 'http://localhost:3000'


const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 200,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  btnColor:{
      backgroundColor: "#447CBA"
  },
  disableBTN:{
    backgroundColor: "#d3d3d3"

  },
  reqBtn:{
    backgroundColor: "#447CBA",
    marginLeft: 30
  }
}));









function DatePicker({location}) {
    const classes = useStyles();
    const [start_date, setStart_Date] = useState('')
    const [end_date, setEnd_Date] = useState('')
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [pending_approval, setPending] = useState('Pending')
    const [data, setData] = useState(null)
    const [show, setShow] = useState(false)
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    
   

    

    

    
    

    

    const handleSubmit = () =>{
        const data = {
             booking: {
             "name": name,
             "start_date": start_date,
             "end_date": end_date,
             "email": email,
             "location": location,
             "pending_approval" : pending_approval
          }   
        }
        axios.post("http://localhost:3000/bookings",data)
        .then(res => {
            
            setData(res.data);
        
            console.log(res.data)
        }).catch(error=>{
            console.error(error)
        })	
    
            handleReset()
            handleClose()
    
    }
    
    

      


    

    const handleReset=()=>{
        setStart_Date('')
        setEnd_Date('')
        setName('')
        setEmail('')
    }

    const handleDate1=(e)=> {
        setStart_Date(e.target.value)
    }

    const handleDate2=(e)=> {
        setEnd_Date(e.target.value)
    }


    const handleName =(e)=> {
        setName(e.target.value)
    }

    const handleEmail =(e)=> {
        setEmail(e.target.value)
    }

    
    const dateConverter = (start_date, end_date) => {
        const newStartDate= new Date(start_date);
        const newEndDate=new Date(end_date);
        const one_day = 1000*60*60*24;
        let result
        result = Math.ceil((newEndDate.getTime()-newStartDate.getTime())/(one_day))
        console.log('date Converter result', result)
        if (result < 0 ) {return 0}
        return result
      }


      var submitBtn;
      if(start_date && end_date && name && email){
          submitBtn =  <Link to="/confirmation"> <Button style={useStyles.btnColor} onClick={handleSubmit}>Submit</Button></Link>
      }else{
        submitBtn =  <Button style={useStyles.disableBTN} disabled>Submit</Button>
      }


      //Form error validations
      let errorMsg = [];
     
      if(!start_date){
         errorMsg.push("Start Date needs to be filled")
      }
      
      if(!end_date){
        errorMsg.push("End date needs to be filled")
      }
      
      if(!name){
        errorMsg.push("Name needs to be filled")
      }
      
      if(!email){
        errorMsg.push("Email needs to be filled")
      }
    

      const messages = errorMsg.map(error => <p style={{color: 'red'}}>{error}</p>)

      let nights = parseInt(dateConverter(start_date,end_date),10)
    

    return (
        <div className="datepicker">
            <form onSubmit={(event)=>handleSubmit(event)}>
             <div className="datepicker__search" style={{display: 'flex', flexDirection:'column', lineHeight: -1}}>
                 
                 
                <br></br>

                <TextField
                    id="start_date"
                    label="Check in"
                    type="date"
                    defaultValue= {new Date()}
                    className={classes.textField}
                    InputLabelProps={{
                    shrink: true,
                    }}
                    value={start_date}
                    onChange={handleDate1}
                    required
                />
                
                &nbsp;
                

               
                <TextField
                    id="end_date"
                    label="Check out"
                    type="date"
                    defaultValue= ""
                    className={classes.textField}
                    InputLabelProps={{
                    shrink: true,
                    }}
                    value={end_date}
                    onChange={handleDate2}
                    required
                />
                &nbsp;
                
                
            </div>  
            <div className='name_email' style={{display: 'flex', flexDirection:'column', lineHeight: -1}}>
                <TextField 
                type='text'
                label='Name'
                id ='name'
                placeholder='John doe'
                value={name}
                onChange = {handleName}
                required
                />
                &nbsp;
               
                 <TextField 
                type='email'
                label='Email'
                id = 'email'
                placeholder='johndoe@email.com'
                value={email}
                onChange = {handleEmail}
                required
                />
                         &nbsp;
                &nbsp;
                    <Button type='button'
                       
                       style={useStyles.reqBtn} data-bs-toggle="modal" 
                       data-bs-target="#exampleModal" 
                       onClick={handleShow}>
                        Request
                    </Button>
                    <Modal show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                    </Modal.Header>
                    <Paper elevation={3} style={{padding: 20}}>
                            <br></br>
                            <h1 style={{textAlign:'center'}}>Confirmation</h1>
                            <h3>Dates: {start_date} to {end_date} </h3>
                            <h4>Total: {nights > 0? nights : 0} Nights</h4>
                            <h4>Destination: {location === 1 ? 'Austin,Tx': 'Destin, Fl'}</h4>
                            <div style={{display:'flex', justifyContent:'space-between'}}>
                                <Button style={useStyles.btnColor} onClick={handleClose}>
                                    Cancel
                                </Button>
                                {submitBtn}
                            </div>
                            {messages}
                        </Paper>
                      </Modal>
               

            </div>
        </form>
        </div>
    )
}

export default DatePicker
