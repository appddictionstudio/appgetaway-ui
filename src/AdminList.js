import React from 'react'
import { DataGrid } from '@material-ui/data-grid';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
}));

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  {
    field: 'fullName',
    headerName: 'Full name',
    description: 'This column has a value getter and is not sortable.',
    sortable: false,
    width: 160,
    valueGetter: (params) =>
      `${params.getValue('firstName') || ''} ${params.getValue('lastName') || ''}`,
  },
    
    {
      field: 'Email',
      headerName: 'Email',
      type: 'number',
      width: 300,
    },
    {
      field: 'Status',
      headerName: 'Status',
      type: 'string',
      width: 300,
    },
    {
      field: 'Location',
      headerName: 'Location',
      type: 'string',
      width: 200,
    },
    {
      field: 'Dates',
      headerName: 'Dates',
      type: 'string',
      width: 200,
    },
    {
      field: 'Nights',
      headerName: 'Number of Nights',
      type: 'number',
      width: 200,
    },
  ];
  
  const rows = [
    { id: 1, lastName: 'Snow', firstName: 'Jon', Status: 'Pending', Location: 'Destin, FL',  Dates: '5/22 - 5/25', Nights: 3, Email: 'jsnow@appddictionstudio.com' },
    { id: 2, lastName: 'Lannister', firstName: 'Cersei', Status: 'Pending',Email: 'clannister@appddictionstudio.com' },
    { id: 3, lastName: 'Lannister', firstName: 'Jaime',Status: 'Pending', Email: 'jlannister@appddictionstudio.com' },
    { id: 4, lastName: 'Stark', firstName: 'Arya', Status: 'Pending',Email: 'astark@appddictionstudio.com' },
    { id: 5, lastName: 'Targaryen', firstName: 'Daenerys', Status: 'Pending',Email: 'dtargaryen@appddictionstudio.com' },
    { id: 6, lastName: 'Melisandre', firstName: null,Status: 'Pending', Email: 'melisandre@appddictionstudio.com' },
    { id: 7, lastName: 'Clifford', firstName: 'Ferrara',Status: 'Pending', Email: 'fclifford@appddictionstudio.com' },
    { id: 8, lastName: 'Frances', firstName: 'Rossini',Status: 'Pending', Email: 'rfrances@appddictionstudio.com' },
    { id: 9, lastName: 'Roxie', firstName: 'Harvey',Status: 'Pending', Email: 'hroxie@appddictionstudio.com' },
  ];

function AdminList() {
  const classes = useStyles();

    return (
        <div className="pending-approvals">
            <div style={{ height: 400, width: '100%' }}>
              <DataGrid rows={rows} columns={columns} pageSize={10} checkboxSelection />
            </div>
            <br>
            </br>
            <div className={classes.root}>
              <Button variant="contained" color="primary">
                Approve
              </Button>
              <Button variant="contained" color="secondary">
                Reject
              </Button>
            </div>
        </div>
    )
}

export default AdminList
